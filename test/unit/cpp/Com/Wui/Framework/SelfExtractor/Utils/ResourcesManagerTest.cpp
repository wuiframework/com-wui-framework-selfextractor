/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::SelfExtractor::Utils::ResourcesManager;

namespace Com::Wui::Framework::SelfExtractor::Utils {
    class ResourcesManagerTest : public testing::Test {
     protected:
        virtual void SetUp() {
        }

        virtual void TearDown() {
        }
    };

    TEST_F(ResourcesManagerTest, unknown) {
    }
}  // namespace Com::Wui::Framework::SelfExtractor::Utils
