/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::SelfExtractor::Utils {
    using Com::Wui::Framework::SelfExtractor::Structures::ResourceInfo;
    using Com::Wui::Framework::XCppCommons::System::IO::FileSystem;

    class ResourcesUpdaterTest : public testing::Test {
     protected:
        virtual void SetUp() {
        }

        virtual void TearDown() {
        }
    };

    TEST_F(ResourcesUpdaterTest, DownloadResource) {
        ResourcesUpdater updater;

        Application::setAppDataPath(FileSystem::getTempPath() + "/com-wui-framework-selfextractor/test");

        ResourceInfo info({
                                  {"name",        "com-wui-framework-builder"},
                                  {"location",    ""},
                                  {"copyOnly",    true},
                                  {"type",        ""},
                                  {"projectName", "com-wui-framework-builder"},
                                  {"releaseName", "Win"},
                                  {"platform",    "app-win-nodejs"},
                                  {"version",     ""}
                          });

        auto asyncProcess = [&](const json $data) {
            if (boost::iequals($data.value("type", ""), "onchange")) {
                int cur = $data["data"].value("currentValue", 0);
                int max = $data["data"].value("rangeEnd", 1);
                std::cout << "!progress: " << cur << "/" << max << std::endl;
            } else if (boost::iequals($data.value("type", ""), "oncomplete")) {
                std::cout << "oncomplete" << std::endl;
            }
        };

        updater.DownloadResource("https://hub.dev.wuiframework.com/xorigin", info, asyncProcess);
    }
}  // namespace Com::Wui::Framework::SelfExtractor::Utils
