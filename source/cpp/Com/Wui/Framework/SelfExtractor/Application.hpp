/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_SELFEXTRACTOR_APPLICATION_HPP_
#define COM_WUI_FRAMEWORK_SELFEXTRACTOR_APPLICATION_HPP_

namespace Com::Wui::Framework::SelfExtractor {
    /**
     * Application class defines main application business logic.
     */
    class Application : public Com::Wui::Framework::XCppCommons::Application {
     public:
        /**
         * Construct default application.
         */
        Application();

        int Run(int $argc, const char **$argv) override;

        /**
         * @return Returns application path. %LOCALAPPDATA% by default.
         */
        static const string &getAppDataPath();

        /**
         * @param $appDataPath Specify new application path.
         */
        static void setAppDataPath(const string &$appDataPath);

        /**
         * Read original file name from version info embedded in executable.
         * @param $path Specify path.
         * @return Returns resolved file name.
         */
        static string ResolveOriginalFileName(const string &$path);

     private:
        bool prepareIntern();

        bool prepareConfig(json &$config);

        shared_ptr<Com::Wui::Framework::SelfExtractor::Utils::ResourcesUpdater> resourcesUpdater = nullptr;
    };
}

#endif  // COM_WUI_FRAMEWORK_SELFEXTRACTOR_APPLICATION_HPP_
