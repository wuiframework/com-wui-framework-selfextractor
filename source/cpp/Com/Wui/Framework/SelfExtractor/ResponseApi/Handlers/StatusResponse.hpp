/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_SELFEXTRACTOR_RESPONSEAPI_HANDLERS_STATUSRESPONSE_HPP_
#define COM_WUI_FRAMEWORK_SELFEXTRACTOR_RESPONSEAPI_HANDLERS_STATUSRESPONSE_HPP_

namespace Com::Wui::Framework::SelfExtractor::ResponseApi::Handlers {
    /**
     * StatusResponse class provides IResponse override to access information from FileSystem and Terminal in SelfExtractor.
     */
    class StatusResponse : public Com::Wui::Framework::XCppCommons::System::ResponseApi::Handlers::BaseResponse {
     public:
        void OnChange(const json &$object) const override;

        void OnComplete(const string &$data) const override;

        void OnComplete(const json &$object, const string &$data) const override;

        /**
         * Register onChange handler.
         * @param $onChange Specify handler.
         */
        void setOnChange(const function<void(const json &)> &$onChange);

        /**
         * Register onComplete handler.
         * @param $onComplete Specify handler.
         */
        void setOnComplete(const function<void(const string &)> &$onComplete);

        /**
         * Register onComplete handler.
         * @param $onComplete Specify handler.
         */
        void setOnComplete(const function<void(const json &, const string &)> &$onComplete);

     private:
        function<void(const json &)> onChange = nullptr;
        function<void(const string &)> onComplete = nullptr;
        function<void(const json &, const string &)> onCompleteB = nullptr;
    };
}

#endif  // COM_WUI_FRAMEWORK_SELFEXTRACTOR_RESPONSEAPI_HANDLERS_STATUSRESPONSE_HPP_
