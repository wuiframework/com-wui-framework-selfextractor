/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_SELFEXTRACTOR_UTILS_TIME_HPP_
#define COM_WUI_FRAMEWORK_SELFEXTRACTOR_UTILS_TIME_HPP_

namespace Com::Wui::Framework::SelfExtractor::Utils {
    /**
     * Time utility class contains advanced features for operations with time.
     */
    class Time : private Com::Wui::Framework::XCppCommons::Interfaces::INonCopyable,
                 private Com::Wui::Framework::XCppCommons::Interfaces::INonMovable {
     public:
        /**
         * Get unix timestamp of specified $time or from current time.
         * @param $time Specify time or leave default to use current time.
         * @return Returns unix timestamp.
         */
        static int getTimestamp(const boost::posix_time::ptime &$time = boost::posix_time::second_clock::universal_time());
    };
}

#endif  // COM_WUI_FRAMEWORK_SELFEXTRACTOR_UTILS_TIME_HPP_
