/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_SELFEXTRACTOR_UTILS_RESOURCESMANAGER_HPP_
#define COM_WUI_FRAMEWORK_SELFEXTRACTOR_UTILS_RESOURCESMANAGER_HPP_

namespace Com::Wui::Framework::SelfExtractor::Utils {
    /**
     * ResourceManager static class provides API to work with embedded resources.
     */
    class ResourcesManager : private Com::Wui::Framework::XCppCommons::Interfaces::INonCopyable,
                             private Com::Wui::Framework::XCppCommons::Interfaces::INonMovable {
     public:
        /**
         * Find configuration embedded in group "CONFIG".
         * @param $name Specify configuration name
         * @return Returns configuration or empty JSON object if not find.
         */
        static json FindConfig(const string &$name);

        /**
         * Read specified config.
         * @param $name Specify config name.
         * @return Returns config in raw string.
         */
        static string ReadConfig(const string &$name);

        /**
         * Read specified resource.
         * @param $name Specify resource name.
         * @param $handler Callback to be called for red data.
         */
        static void ReadResource(const string &$name, std::function<void(const char *, unsigned int)> const &$handler);
    };
}

#endif  // COM_WUI_FRAMEWORK_SELFEXTRACTOR_UTILS_RESOURCESMANAGER_HPP_
