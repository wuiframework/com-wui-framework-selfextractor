/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_SELFEXTRACTOR_ENUMS_TEXTJUSTIFYTYPE_HPP_
#define COM_WUI_FRAMEWORK_SELFEXTRACTOR_ENUMS_TEXTJUSTIFYTYPE_HPP_

namespace Com::Wui::Framework::SelfExtractor::Enums {
    /**
     * TextJustifyType enum provides definition of horizontal text alignment in text user controls.
     */
    class TextJustifyType : public Com::Wui::Framework::XCppCommons::Primitives::BaseEnum<TextJustifyType> {
     WUI_ENUM_DECLARE(TextJustifyType)

     public:
        static const TextJustifyType LEFT;
        static const TextJustifyType CENTER;
        static const TextJustifyType RIGHT;
    };
}

#endif  // COM_WUI_FRAMEWORK_SELFEXTRACTOR_ENUMS_TEXTJUSTIFYTYPE_HPP_
