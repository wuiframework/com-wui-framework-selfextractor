/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_SELFEXTRACTOR_STRUCTURES_SELFEXTRACTORARGS_HPP_
#define COM_WUI_FRAMEWORK_SELFEXTRACTOR_STRUCTURES_SELFEXTRACTORARGS_HPP_

namespace Com::Wui::Framework::SelfExtractor::Structures {
    /**
     * SelfExtractorArgs class provides program options used in WuiSelfExtractor application.
     */
    class SelfExtractorArgs
            : public Com::Wui::Framework::XCppCommons::Structures::ProgramArgs {
     public:
        /**
         * Constructs args instance with configuration of all program options.
         */
        SelfExtractorArgs();

        /**
         * @return Returns target application path.
         */
        const string &getTarget() const;

        /**
         * @param $value Specify target application path.
         */
        void setTarget(const string &$value);

        /**
         * @return Returns true if configuration print is required, false otherwise.
         */
        bool isPrintConfiguration() const;

        /**
         * @param $printConfiguration Set true to print embedded configuration, false do not print.
         */
        void setPrintConfiguration(bool $printConfiguration);

        /**
         * @return Returns true if cache should be disabled and all resources and configs will be downloaded.
         */
        bool isDisableCache() const;

        /**
         * @param $disableCache Set true to disable cache, false otherwise.
         */
        void setDisableCache(bool $disableCache);

        /**
         * @return Returns true if headless mode is required (SelfExtractor without GUI)
         */
        bool isHeadless() const;

        /**
         * @param $headless Set true to enable headless mode (SelfExtractor without GUI)
         */
        void setHeadless(bool $headless);

        /**
         * @return Returns arguments string for application. Preconfigured executable will be invoked with '--appArgs="<CLI args>"'.
         */
        const string &getAppArgs() const;

        /**
         * @param $appArgs Specify arguments string for application. Preconfigured executable will be invoked with '--appArgs="<CLI args>"'.
         */
        void setAppArgs(const string &$appArgs);

        /**
         * @return Returns true if configured executable execute should be skipped, false otherwise.
         */
        bool isSkipExecute() const;

        /**
         * @param $skipExecute Set true to skip configured executable execute, false otherwise.
         */
        void setSkipExecute(bool $skipExecute);

        /**
         * @return Returns true if selfextractor should print application path to stdOut.
         */
        bool isPrintAppPath() const;

        /**
         * @param $printAppPath Set true to print application path to stdOut, false otherwise.
         */
        void setPrintAppPath(bool $printAppPath);

     private:
        string target;
        bool printConfiguration = false;
        bool disableCache = false;
        bool headless = false;
        string appArgs;
        bool skipExecute = false;
        bool printAppPath = false;
    };
}

#endif  // COM_WUI_FRAMEWORK_SELFEXTRACTOR_STRUCTURES_SELFEXTRACTORARGS_HPP_
