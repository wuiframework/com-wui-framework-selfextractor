/** WARNING: this file has been automatically generated from C++ interfaces and classes, which exist in this package. */
// NOLINT (legal/copyright)

#ifndef COM_WUI_FRAMEWORK_SELFEXTRACTOR_INTERFACESMAP_HPP_  // NOLINT
#define COM_WUI_FRAMEWORK_SELFEXTRACTOR_INTERFACESMAP_HPP_

namespace Com {
    namespace Wui {
        namespace Framework {
            namespace SelfExtractor {
                class Application;
                class Loader;
                namespace Enums {
                    class TextJustifyType;
                }
                namespace Primitives {
                    class BaseGuiObject;
                }
                namespace ResponseApi {
                    namespace Handlers {
                        class StatusResponse;
                    }
                }
                namespace Structures {
                    class CacheEntry;
                    class Color;
                    class Font;
                    class Position;
                    class ResourceInfo;
                    class SelfExtractorArgs;
                    class Size;
                }
                namespace UserControls {
                    class Image;
                    class Label;
                    class ProgressBar;
                    class SplashScreen;
                }
                namespace Utils {
                    class CacheManager;
                    class JSON;
                    class Localization;
                    class ResourcesManager;
                    class ResourcesUpdater;
                    class Time;
                }
            }
        }
    }
}

#endif  // COM_WUI_FRAMEWORK_SELFEXTRACTOR_INTERFACESMAP_HPP_  // NOLINT
