/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_SELFEXTRACTOR_HPP_  // NOLINT
#define COM_WUI_FRAMEWORK_SELFEXTRACTOR_HPP_

#include <boost/asio.hpp>
#include <boost/lockfree/spsc_queue.hpp>

#ifdef WIN_PLATFORM

#include <objidl.h>
#include <gdiplus.h>

#endif

#include "../../dependencies/com-wui-framework-xcppcommons/source/cpp/reference.hpp"
#include "interfacesMap.hpp"
#include "Com/Wui/Framework/SelfExtractor/sourceFilesMap.hpp"

#include "Com/Wui/Framework/SelfExtractor/Structures/Position.hpp"
#include "Com/Wui/Framework/SelfExtractor/Structures/Size.hpp"
#include "Com/Wui/Framework/SelfExtractor/Structures/ResourceInfo.hpp"

// generated-code-start
#include "Com/Wui/Framework/SelfExtractor/Application.hpp"
#include "Com/Wui/Framework/SelfExtractor/Enums/TextJustifyType.hpp"
#include "Com/Wui/Framework/SelfExtractor/Primitives/BaseGuiObject.hpp"
#include "Com/Wui/Framework/SelfExtractor/ResponseApi/Handlers/StatusResponse.hpp"
#include "Com/Wui/Framework/SelfExtractor/Structures/CacheEntry.hpp"
#include "Com/Wui/Framework/SelfExtractor/Structures/Color.hpp"
#include "Com/Wui/Framework/SelfExtractor/Structures/Font.hpp"
#include "Com/Wui/Framework/SelfExtractor/Structures/SelfExtractorArgs.hpp"
#include "Com/Wui/Framework/SelfExtractor/UserControls/Image.hpp"
#include "Com/Wui/Framework/SelfExtractor/UserControls/Label.hpp"
#include "Com/Wui/Framework/SelfExtractor/UserControls/ProgressBar.hpp"
#include "Com/Wui/Framework/SelfExtractor/UserControls/SplashScreen.hpp"
#include "Com/Wui/Framework/SelfExtractor/Utils/CacheManager.hpp"
#include "Com/Wui/Framework/SelfExtractor/Utils/JSON.hpp"
#include "Com/Wui/Framework/SelfExtractor/Utils/Localization.hpp"
#include "Com/Wui/Framework/SelfExtractor/Utils/ResourcesManager.hpp"
#include "Com/Wui/Framework/SelfExtractor/Utils/ResourcesUpdater.hpp"
#include "Com/Wui/Framework/SelfExtractor/Utils/Time.hpp"
// generated-code-end

#include "Com/Wui/Framework/SelfExtractor/Loader.hpp"

#endif  // COM_WUI_FRAMEWORK_SELFEXTRACTOR_HPP_  NOLINT
